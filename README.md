```yamlex
endpoints: [127.0.0.1:2379]
timeout: 2
keybase: /sample_keybase
```

修改 `keybase` 为实际的 _KeyPath Base_

* _GET /prefix/sub/key/path_

```
etcdctl get http://{endpoint}/{keybase}/{self ip}/sub/key/path --prefix
```

* _GET /accurate/sub/key/path_

```
etcdctl get http://{endpoint}/{keybase}/{self ip}/sub/key/path
```

* _Response_

```json
{
  "count": int,
  "results": [
    {
      "key": string,
      "value": string,
      "create_revision": int64,
      "mod_revision": int64,
      "version": int64
    },
    ...
  ]
}
```

package config

import (
	"github.com/go-chassis/go-chassis/core/lager"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type EtcdConfig struct {
	Endpoints []string `yaml:"endpoints"`
	Timeout  int    `yaml:"timeout"`
	KeyBase string `yaml:"keybase"`
}

var EtcdCfg EtcdConfig

type Lager struct {
	Writers        string `yaml:"writers"`
	LoggerLevel    string `yaml:"logger_level"`
	LoggerFile     string `yaml:"logger_file"`
	LogFormatText  bool   `yaml:"log_format_text"`
	RollingPolicy  string `yaml:"rollingPolicy"`
	LogRotateDate  int    `yaml:"log_rotate_date"`
	LogRotateSize  int    `yaml:"log_rotate_size"`
	LogBackupCount int    `yaml:"log_backup_count"`
}

var LagerCfg Lager

func init() {
	var err error

	content, err := ioutil.ReadFile("conf/lager.yaml")
	if err != nil {
		panic(err)
	}

	err = yaml.UnmarshalStrict(content, &LagerCfg)
	if err != nil {
		panic(err)
	}

	lager.Initialize(
		LagerCfg.Writers,
		LagerCfg.LoggerLevel,
		LagerCfg.LoggerFile,
		LagerCfg.RollingPolicy,
		LagerCfg.LogFormatText,
		LagerCfg.LogRotateDate,
		LagerCfg.LogRotateSize,
		LagerCfg.LogBackupCount,
	)

	lager.Logger.Infof("new initial")

	content, err = ioutil.ReadFile("conf/etcd.yaml")
	if err != nil {
		lager.Logger.Errorf("%s", err)
		panic(err)
	}

	err = yaml.UnmarshalStrict(content, &EtcdCfg)
	if err != nil {
		lager.Logger.Errorf("%s", err)
		panic(err)
	}
}

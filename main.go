package main

import (
	"github.com/go-chassis/go-chassis"
	//_ "gitlab.com/xfwduke/strict-etcd/internal/config"
	"gitlab.com/xfwduke/strict-etcd/modules/strict_etcd"
)

func main() {

	chassis.RegisterSchema("rest", &strict_etcd.DummyModule{})
	if err := chassis.Init(); err != nil {
		panic(err)
	}
	chassis.Run()
}

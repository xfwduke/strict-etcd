package strict_etcd

import (
	"context"
	"github.com/coreos/etcd/clientv3"
	"github.com/go-chassis/go-chassis/core/lager"
	"gitlab.com/xfwduke/strict-etcd/internal/config"
	"time"
)

var etcdClient *clientv3.Client
//var etcdBase = "/gcs_prometheus_exporter"

func init() {
	var err error

	etcdClient, err = clientv3.New(clientv3.Config{
		Endpoints: config.EtcdCfg.Endpoints,
		DialTimeout: time.Duration(config.EtcdCfg.Timeout) * time.Second,
	})
	if err != nil {
		lager.Logger.Errorf("%s", err)
		panic(err)
	}
}

func fetch(keyPath string, withPrefix bool) (rr *restResp, err error) {
	rr = &restResp{
		Count: 0,
	}

	lager.Logger.Debugf("keypath: %s", keyPath)
	lager.Logger.Debugf("withprefix: %t", withPrefix)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	var options []clientv3.OpOption
	if withPrefix {
		options = append(options, clientv3.WithPrefix())
	}

	resp, err := etcdClient.Get(ctx, keyPath, options...)
	cancel()
	if err != nil {
		lager.Logger.Errorf("%s", err)
		return rr, err
	}

	for _, e := range resp.Kvs {
		rr.Result = append(rr.Result, etcdResp{
			Key:            string(e.Key),
			Value:          string(e.Value),
			Version:        e.Version,
			ModRevision:    e.ModRevision,
			CreateRevision: e.CreateRevision,
		})
		rr.Count += 1
	}

	return rr, nil
}

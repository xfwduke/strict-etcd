package strict_etcd

import (
	"github.com/go-chassis/go-chassis/core/lager"
	"github.com/go-chassis/go-chassis/server/restful"
	"gitlab.com/xfwduke/strict-etcd/internal/config"
	"net/http"
	"path"
	"strings"
)

func (c *DummyModule) Handler(b *restful.Context) {
	caller := strings.Split(b.ReadRequest().RemoteAddr, ":")[0]

	subPath := b.ReadPathParameter("subpath")
	keyPath := path.Join(config.EtcdCfg.KeyBase, caller, subPath)

	rr, err := fetch(keyPath, b.ReadPathParameter("withprefix") == "prefix")
	if err != nil {
		lager.Logger.Errorf("%s", err)
	}

	b.WriteJSON(rr, "application/json")
}

func (c *DummyModule) URLPatterns() []restful.Route {
	return []restful.Route{
		{http.MethodGet, "/{withprefix:^(prefix|accurate)$}/{subpath:*}", "Handler"},
		{http.MethodGet, "/{withprefix:^(prefix|accurate)$}/", "Handler"},
	}
}

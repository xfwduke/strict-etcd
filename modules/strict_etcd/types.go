package strict_etcd

type DummyModule struct {
}

type etcdResp struct {
	Key            string `json:"key"`
	Value          string `json:"value"`
	CreateRevision int64  `json:"create_revision"`
	ModRevision    int64  `json:"mod_revision"`
	Version        int64  `json:"version"`
}

type restResp struct {
	Count  int        `json:"count"`
	Result []etcdResp `json:"results"`
}
